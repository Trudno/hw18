﻿#include <iostream>
#include <vector>
#include <string>
using namespace std;
template<typename N>
class Stack
{
public:
    void push(N);
    N pop();
    void show();
private:
    vector <N> v;
};

int main() {
    Stack<int> a;
    a.push(1);  a.push(2);  a.push(3);
    a.show();
    cout << "poped:" << a.pop() << endl;
    a.show();

    Stack<string> b;
    b.push("4"); b.push("5");
    b.show();
    cout << "poped:" << b.pop() << endl;
    b.show();

    return 0;
}

template<class N> void Stack<N>::push(N elem)
{
    v.push_back(elem);
}

template<class N> N Stack<N>::pop()
{
    N elem = v.back();
    v.pop_back();
    return elem;
}
template<class N> void Stack<N>::show()
{
    cout << "stack:";
    for (auto e : v) cout << e << " ";
    cout << endl;
}

 
